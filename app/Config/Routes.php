<?php

use App\Controllers\Admin;
use App\Controllers\Api\UserApi;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::register');
$routes->match(['GET', 'POST'],'/register', 'Home::register', ['as' => 'register']);

$routes->group('admin', static function ($routes) {
    $routes->get('users', [Admin::class, 'userList']);
    $routes->get('users/(:num)', [Admin::class, 'delete']);
});

$routes->group('api', static function ($routes) {
    $routes->get('users', [UserApi::class, 'getUsers']);
    $routes->post('users', [UserApi::class, 'createUser']);
    $routes->put('users/(:num)', [UserApi::class, 'updateUser/$1']);
    $routes->delete('users/(:num)', [UserApi::class, 'deleteUser/$1']);
});
