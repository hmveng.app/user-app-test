<?php

namespace App\Models;

use App\Entities\User;
use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected bool $updateOnlyChanged = true;
    protected $allowedFields = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'postal_address',
        'job_status',
        'logged_at'
    ];
    protected $dateFormat = 'datetime';
    protected $returnType = User::class;

    public function getUsers(int $page, int $perPage): array
    {
        $this->builder()->select('user.*');

        return [
            'users'  => $this->paginate($perPage, 'default', $page),
            'page' => $page,
            'limit' => $perPage,
            'total' => $this->countAllResults(),
        ];
    }
    
    // public function getNews($slug = false)
    // {
    //     if ($slug === false) {
    //         return $this->findAll();
    //     }

    //     return $this->where(['slug' => $slug])->first();
    // }
}