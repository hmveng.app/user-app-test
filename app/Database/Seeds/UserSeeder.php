<?php

namespace App\Database\Seeds;

use Faker\Factory;
use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    const USER_MAX = 1000;

    public function run()
    {
        for ($i = 1; $i <= self::USER_MAX; $i++) {
            $this->db->table('user')->insert($this->generateUser());
        }
    }

    private function generateUser(): array
    {
        $faker = Factory::create();
        
        return [
            'firstname' => $faker->firstName(),
            'lastname'=> $faker->lastName(),
            'email' => $faker->email(),
            'phone' => $faker->phoneNumber(),
            'postal_address' => $faker->address(),
            'job_status' => $this->getJobStatus(),
            'logged_at' => $this->getLoggedAt()->format('Y-m-d H:i:s'),
        ];
    }

    private function getLoggedAt(): \DateTimeInterface
    {
        $dates = [
            new \DateTime(),
            (new \DateTime())->modify('-2 day'),
            (new \DateTime())->modify('-5 day'),
            (new \DateTime())->modify('-1 month'),
            (new \DateTime())->modify('-36 month')
        ];

        return $dates[random_int(0, count($dates) - 1)];
    }

    private function getJobStatus(): string
    {
        $jobsStatus = ['salarié cadre', 'salarié non-cadre', 'profession libérale', 'fonction publique'];

        return $jobsStatus[random_int(0, count($jobsStatus) - 1)];
    }
}