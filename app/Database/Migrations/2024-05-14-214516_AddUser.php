<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUser extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'firstname' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'lastname' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
                'unique' => true
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'postal_address' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'job_status' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'logged_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'logged_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('user');
    }

    public function down()
    {
        $this->forge->dropTable('user');
    }
}
