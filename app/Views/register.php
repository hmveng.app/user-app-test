<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to CodeIgniter 4!</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="py-5">
            <h2>Création d'un utilisateur</h2>
        </div>

        <div class="row">
            <div class="col-md-8 order-md-1">
                <?php if (session()->getFlashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->getFlashdata('success') ?>!
                    </div>
                <?php endif; ?>

                <?php if (session()->getFlashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->getFlashdata('error') ?>!
                    </div>
                <?php endif; ?>
            </div>
        </div>

        
        <div class="row">

            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Vos informations</h4>
                <form class="needs-validation" novalidate="" method="post" >
                    <div class="mb-3">
                        <label for="firstname">Prénom </label>
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Votre prénom">
                    </div>

                    <div class="mb-3">
                        <label for="lastname">Nom </label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Votre nom">
                    </div>

                    <div class="mb-3">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com">
                    </div>

                    <div class="mb-3">
                        <label for="phone">Numéro de téléphone </label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Votre numéro de téléphone">
                    </div>

                    <div class="mb-3">
                        <label for="postal_address">Adresse postale </label>
                        <input type="text" class="form-control" name="postal_address" id="postal_address" placeholder="Votre statut professionnel">
                    </div>

                    <div class="mb-3">
                        <label for="job_status">Statut professionnel </label>
                        <input type="text" class="form-control" name="job_status" id="job_status" placeholder="Votre statut professionnel">
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Enregister</button>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
</body>
</html>
