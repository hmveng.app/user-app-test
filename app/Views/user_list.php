<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to CodeIgniter 4!</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="py-5">
            <h2>Liste des utilisateurs</h2>
        </div>

        <div class="row">

            <div class="col-md-12 order-md-1">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Prénom</th>
                            <th scope="col">Adresse email</th>
                            <th scope="col">Numéro de téléphone</th>
                            <th scope="col">Statut professionnel</th>
                            <th scope="col">Addresse postale</th>
                            <th scope="col">Date dernière connexion</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach( $users as $user) : ?>
                        <tr>
                            <th scope="row"> <?= $user->id; ?></th>
                            <td><?= $user->lastname; ?></td>
                            <td> <?= $user->firstname; ?> </td>
                            <td> <?= $user->email; ?> </td>
                            <td> <?= $user->phone; ?> </td>
                            <td> <?= $user->job_status; ?> </td>
                            <td> <?= $user->postal_address; ?> </td>
                            <td> <?= $user->logged_at->date; ?> </td>
                            <td>
                                
                                <a href="<?= url_to('Admin::delete', $user->id)  ?>" class="btn btn-danger btn-sm" tabindex="-1" role="button" aria-disabled="true">
                                    Supprimer
                                </a>
                            </td>
                        </tr>
                        <?php endforeach  ?>
                    </tbody>
                </table>

                <?= $pager_links ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
</body>
</html>
