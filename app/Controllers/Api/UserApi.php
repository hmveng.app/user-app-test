<?php

namespace App\Controllers\Api;

use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;

class UserApi extends BaseController
{
    use ResponseTrait;
    
    public function getUsers()
    {
        $page = (int) ($this->request->getGet('page') ?? 1);
        $limit = (int) ($this->request->getGet('limit') ?? 20);

        $model = model(UserModel::class);
        $data = $model->getUsers($page, $limit);

        return $this->respond($data, 200);
    }

    public function createUser() 
    {
        $validation = \Config\Services::validation();
        $validation->setRules([
            'firstname' => 'required',
            'lastname'=> 'required',
            'email' => 'required|valid_email|is_unique[user.email]',
            'phone' => 'required',
            'postal_address' => 'required',
            'job_status' => 'required',
        ]);

        $payload = $this->request->getJSON(true);

        if(true === $validation->run($payload)) {
            $data = $validation->getValidated();

            $model = new UserModel();
            $user = $model->insert($data);

            return $this->respondCreated([
                "message" => "User created successfully.",
                "userId" => $user
            ]);
        }

        return $this->failValidationErrors($validation->getErrors());
    }

    public function updateUser(int $id)
    {
        $model = new UserModel();
        $user = $model->find($id);
        if( null === $user) {
            return $this->failNotFound("User not found for update.");
        }

        $validation = \Config\Services::validation();
        $validation->setRules([
            'id' => 'required',
            'firstname' => 'required',
            'lastname'=> 'required',
            'email' => 'required|valid_email|is_unique[user.email, id, {id}]',
            'phone' => 'required',
            'postal_address' => 'required',
            'job_status' => 'required',
        ]);

        $payload = $this->request->getJSON(true);
        $payload['id'] = $id;

        if(true === $validation->run($payload)) {
            $data = $validation->getValidated();
            unset($data['id']);

            $user = $model->update($id, $data);

            return $this->respondCreated([
                "message" => "User updated successfully.",
                "userId" => $user
            ]);
        }

        return $this->failValidationErrors($validation->getErrors());
    }

    public function deleteUser(int $id)
    {
        $model = new UserModel();
        $user = $model->find($id);
        if( null === $user) {
            return $this->failNotFound("User not found for removal.");
        }

        $model->delete($user->id);

        return $this->respondCreated([
            "message" => "User deleted successfully."
        ]);
    }
}