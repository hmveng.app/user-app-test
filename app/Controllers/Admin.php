<?php

namespace App\Controllers;

use CodeIgniter\HTTP\RedirectResponse;

class Admin extends BaseController
{
    public function userList(): string
    {
        $page = (int) ($this->request->getGet('page') ?? 1);

        $client = \Config\Services::curlrequest(); 
        $uri = base_url().'api/users?page='. $page;

        $response = $client->request('GET', $uri);
        
        $data = json_decode($response->getBody());

        $pager = \Config\Services::pager();
        $pager_links = $pager->makeLinks($page, $data->limit, $data->total, 'custom_pagination');

        return view('user_list', [
            'users' => $data->users,
            'limit' => $data->limit,
            'total' => $data->total,
            'pager_links' => $pager_links,
        ]);
    }

    public function delete(int $id): RedirectResponse
    {
        $uri = base_url().'api/users/'. $id;
        $client = \Config\Services::curlrequest();
        $client->request('DELETE', $uri);

        return redirect()->to('admin/users');
    }
}
