<?php

namespace App\Controllers;

class Home extends BaseController
{

    public function register(): string
    {
        if( $this->request->is('post')) {
             
             $session = \Config\Services::session(); 
             $client = \Config\Services::curlrequest(); 
             $uri = base_url().'api/users';

             try {
                $response = $client->request(
                    'POST',
                    $uri, 
                    [
                        'debug' => true,
                        'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                        'json' => $this->request->getGetPost()
                    ]
                );
                
                $body = json_decode($response->getBody());
                $session->setFlashdata('success', $body->message);
             } catch (\CodeIgniter\HTTP\Exceptions\HTTPException $e) {
                $session->setFlashdata('error', $e->getMessage());
             }
        }

        return view('register');
    }
}
