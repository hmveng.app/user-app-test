<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class User extends Entity
{
    protected $attributes = [
        'id' => null,
        'firstname' => null,
        'lastname'=> null,
        'email' => null,
        'phone' => null,
        'postal_address' => null,
        'job_status' => null,
        'logged_at' => null,
    ];

    protected $casts = [
        'id' => 'int',
        'firstname' => 'string',
        'lastname'=> 'string',
        'email' => 'string',
        'phone' => 'string',
        'postal_address' => 'string',
        'job_status' => 'string',
        'logged_at' => 'datetime',
    ];

    protected $dates = ['logged_at'];

    
}