# Installation

## Prerequis

Avoir docker installé localement

## Setup

Copie `env` en `.env` et modification des variables suivantes:


```bash
  CI_ENVIRONMENT = development
  database.default.hostname = db
  database.default.database = av_db
  database.default.username = root
  database.default.password = 
```

## Lancez la commande suivante pour installer l'environnement

PS: l'installation des données met du temps (1000 utilisateurs)

```bash
  make build-app
```

## Ouverture de l'application

APP
```bash
  BO: Listes des utilisateurs: http://localhost:9000/admin/users
  FO: Création d'un utilisateur: http://localhost:9000/register
```

phpMyAdmin: 
```bash
  PMA: http://localhost:9002/
```

API:

- Création utilisateur
```bash
  POST : http://localhost:9000/api/users
```
```bash
  {
    "firstname": "John",
    "lastname": "Doe",
    "email": "john.doe@test.com",
    "phone": "0600000000",
    "postal_address": "12 Bd xxx, 44000 Nantes",
    "job_status": "Cadre"
  }
```
- Modification utilisateur
```bash
  PUT : http://localhost:9000/api/users/1
```
```bash
  {
    "firstname": "Martin",
    "lastname": "Doe",
    "email": "john.doe@test.com",
    "phone": "0600000000",
    "postal_address": "12 Bd xxx, 44000 Nantes",
    "job_status": "Cadre"
  }
```

- Suppression utilisateur
```bash
  DELETE : http://localhost:9000/api/users/1
```

- Liste des utilisateurs
```bash
  GET : http://localhost:9000/api/users?page=1&limit=20
```

