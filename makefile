#---CI--#
CONSOLE = php spark
DOCKER_COMPOSE = docker-compose
DOCKER_EXEC = docker exec
APP_CONTAINER_NAME = av_app
#------------#


##
## HELP
help: ## Show this help.
	@echo "Command helper"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


# install package in host
install-packages:
	$(DOCKER_EXEC) $(APP_CONTAINER_NAME) composer install --no-scripts
# @composer install --no-scripts

# build
build:
	$(DOCKER_COMPOSE) up -d

# build
install-db:
	$(DOCKER_EXEC) $(APP_CONTAINER_NAME) $(CONSOLE) db:create av_db
	$(DOCKER_EXEC) $(APP_CONTAINER_NAME) $(CONSOLE) migrate
	$(DOCKER_EXEC) $(APP_CONTAINER_NAME) $(CONSOLE) db:seed UserSeeder

# build app (firt time)
build-app:
	@make build
	@make install-packages
	@make install-db

.PHONY: info install-packages build install-db build-app


